# Tutor

Keep track of learning practice and discovery goals. Helps along the way.

# Projected Tech
* Gatsby
* Material Design UI
* Lambda Functions
* Airtable
* Apollo

# Projected Features
Login/Profile
Create/Manage Practice Routines
Major/Minor Subjects
Display Graphs/Graphics
Time Budget Settings
Practice Scheduling/Tracking
Notifications
Resources/Assets
